class ListKarakters {
  final String thumbnail;
  final String name;

  ListKarakters({this.thumbnail, this.name});

  factory ListKarakters.fromJson(Map<String, dynamic> json) {
    return ListKarakters(
      thumbnail: json['thumbnail'],
      name: json['name'],
    );
  }
}
