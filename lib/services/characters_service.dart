import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

import '../models/m_karakters.dart';

Future<List<ListKarakters>> fetchCharacters() async {
  var response;
  try{
    response = await http.get('https://api.bandungdev.my.id/bumilangit/karakter.php');

    var mapRes = json.decode(response.body);

    return compute(parsePosts, json.encode(mapRes));
  }catch(E){
    print('E '+E.toString());
    return E;
  }
}

List<ListKarakters> parsePosts(String responseBody) {
  final parsed = json.decode(responseBody).cast<Map<String, dynamic>>();
  return parsed.map<ListKarakters>((json) => ListKarakters.fromJson(json)).toList();
}