import 'dart:convert';

import 'package:bumi_langit/models/m_karakters.dart';
import 'package:bumi_langit/services/characters_service.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Karakters extends StatefulWidget {
  @override
  _KaraktersState createState() => _KaraktersState();
}

class _KaraktersState extends State<Karakters> {
  Map datanew;
  //List karakterList;
  List<ListKarakters> karakterList;

  Future getData() async {
    final response = await http.get('https://api.bandungdev.my.id/bumilangit/karakter.php');

    var data = json.decode(response.body);
    setState(() {
      for (var i = 0; i < data.length; i++) {
        karakterList.add(ListKarakters.fromJson(data[i]));
      }
    });
  }

  @override
  void initState() {
    super.initState();
    //getData();
    fetchCharacters().then((onValue) {
      setState(() {
        karakterList = onValue;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 100.0,
      child: ListView.builder(
        padding: EdgeInsets.only(left: 10.0),
        scrollDirection: Axis.horizontal,
        itemCount: karakterList.length,
        itemBuilder: (BuildContext context, int index) {
          return GestureDetector(
            onTap: () {},
            child: Padding(
              padding: EdgeInsets.all(10.0),
              child: Column(
                children: <Widget>[
                  /*CircleAvatar(
                    radius: 30.0,
                    backgroundImage: AssetImage(karakterList[index].thumbnail),
                  ),*/
                  new Container(
                      width: 60.0,
                      height: 60.0,
                      decoration: new BoxDecoration(
                          border: new Border.all(
                              color: Colors.blue,
                              width: 1.5),
                          shape: BoxShape.circle,
                          image: new DecorationImage(
                              fit: BoxFit.fill,
                              image: new NetworkImage(
                                  karakterList[index].thumbnail
                              )))),
                  SizedBox(
                    height: 6.0,
                  ),
                  Text(
                    karakterList[index].name,
                    style: TextStyle(
                        color: Colors.blueGrey, fontSize: 12.0, fontWeight: FontWeight.bold),
                  ),
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
