import 'package:bumi_langit/models/m_home.dart';
import 'package:bumi_langit/screen/karakters.dart';
import 'package:flutter/material.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                height: MediaQuery.of(context).size.width * 0.7,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(30.0),
                        bottomRight: Radius.circular(30.0)),
                    boxShadow: [
                      BoxShadow(
                          blurRadius: 6.0,
                          color: Colors.black26,
                          offset: Offset(0.0, 3.0))
                    ]),
                child: ClipRRect(
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(30.0),
                      bottomRight: Radius.circular(30.0)),
                  child: Image(
                    image: AssetImage(imageCover),
                    fit: BoxFit.cover,
                  ),
                ),
              )
            ],
          ),
          Container(
            height: 200,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text(
                        'Karakter Bumi Langit',
                        style: TextStyle(
                            fontSize: 15.0,
                            fontWeight: FontWeight.bold,
                            letterSpacing: 1.0),
                      ),
                      IconButton(
                        icon: Icon(Icons.more_horiz),
                        iconSize: 20.0,
                        color: Colors.black,
                        onPressed: () {},
                      )
                    ],
                  ),
                ),
                Karakters()
              ],
            ),
          )
        ],
      ),
    );
  }
}
